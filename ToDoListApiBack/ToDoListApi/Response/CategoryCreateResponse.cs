﻿namespace ToDoListApi.Response
{
    public class CategoryCreateResponse : BaseResponse
    {
        public string Content { get; set; }
    }
}
