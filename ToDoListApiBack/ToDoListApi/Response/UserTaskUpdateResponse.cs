﻿namespace ToDoListApi.Response
{
    public class UserTaskUpdateResponse : BaseResponse
    {
        public string Content { get; set; }
    }
}
