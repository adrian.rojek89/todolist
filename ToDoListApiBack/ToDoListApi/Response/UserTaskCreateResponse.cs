﻿namespace ToDoListApi.Response
{
    public class UserTaskCreateResponse : BaseResponse
    {
        public string Content { get; set; }
    }
}
