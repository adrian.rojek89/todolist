﻿namespace ToDoListApi.Response
{
    public class CategoryUpdateResponse : BaseResponse
    {
        public string Content { get; set; }
    }
}
