﻿namespace ToDoListApi.Response
{
    public abstract class BaseResponse
    {
        public bool Error { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
