﻿namespace ToDoListApi.Response
{
    public class CategoryUpdatePlaceResponse : BaseResponse
    {
        public string Content { get; set; }
    }
}
