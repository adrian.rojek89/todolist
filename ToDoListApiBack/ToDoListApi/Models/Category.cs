﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoListApi.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Place { get; set; }

        public List<UserTask> UserTasks { get; set; }
    }
}
