﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoListApi.Data;
using ToDoListApi.Models;

namespace ToDoListApi.Services
{
    public class UserTaskService : IUserTaskService
    {
        private readonly ApplicationDbContext _context;

        public UserTaskService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CreateAsync(UserTask userTask)
        {
            await _context.UserTasks.AddAsync(userTask);
            int created = await _context.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            UserTask userTaskToDelete = await GetAsync(id);
            if (userTaskToDelete == null)
            {
                return false;
            }

            _context.UserTasks.Remove(userTaskToDelete);
            int deleted = await _context.SaveChangesAsync();
            return deleted > 0;
        }

        public async Task<List<UserTask>> GetAllAsync()
        {
            return await _context.UserTasks.ToListAsync();
        }

        public async Task<UserTask> GetAsync(int id)
        {
            return await _context.UserTasks.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(UserTask userTask)
        {
            _context.UserTasks.Update(userTask);
            int update = await _context.SaveChangesAsync();
            return update > 0;
        }
    }

    public interface IUserTaskService
    {
        Task<UserTask> GetAsync(int id);
        Task<List<UserTask>> GetAllAsync();
        Task<bool> DeleteAsync(int id);
        Task<bool> CreateAsync(UserTask userTask);
        Task<bool> UpdateAsync(UserTask userTask);
    }
}
