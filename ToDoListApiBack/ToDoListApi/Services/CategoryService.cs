﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoListApi.Data;
using ToDoListApi.Models;

namespace ToDoListApi.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ApplicationDbContext _context;

        public CategoryService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CreateAsync(Category category)
        {
            await _context.Categorys.AddAsync(category);
            int created = await _context.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            Category categoryToDelete = await GetAsync(id);
            if (categoryToDelete == null)
            {
                return false;
            }

            _context.Categorys.Remove(categoryToDelete);
            int deleted = await _context.SaveChangesAsync();
            return deleted > 0;
        }

        public async Task<List<Category>> GetAllAsync()
        {
            return await _context.Categorys.ToListAsync();
        }

        public async Task<List<Category>> GetAllWithUserTaskAsync()
        {
            return await _context.Categorys
                .Include(category => category.UserTasks)
                .ToListAsync(); 
        }


        public async Task<Category> GetAsync(int id)
        {
            return await _context.Categorys.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> UpdateAsync(Category category)
        {
            _context.Categorys.Update(category);
            int update = await _context.SaveChangesAsync();
            return update > 0;
        }

        public async Task<bool> UpdatePlaceAsync(List<Category> categorys)
        {
            foreach (Category cat in categorys)
            {
                Category lastCategory = await _context.Categorys.AsNoTracking().FirstOrDefaultAsync(x => x.Id == cat.Id);

                if(lastCategory == null)
                {
                    continue;
                }

                lastCategory.Place = cat.Place;
                _context.Categorys.Update(lastCategory);
            }

            int update = await _context.SaveChangesAsync();
            return update > 0;
        }

        public async Task<int> GetLastPlaceAsync()
        {
            Category lastCategory = await _context.Categorys.AsNoTracking().LastOrDefaultAsync();

            if(lastCategory == null)
            {
                return 0;
            }

            return lastCategory.Place;
        }
    }

    public interface ICategoryService
    {
        Task<Category> GetAsync(int id);
        Task<List<Category>> GetAllAsync();
        Task<List<Category>> GetAllWithUserTaskAsync();
        Task<bool> DeleteAsync(int id);
        Task<bool> CreateAsync(Category category);
        Task<bool> UpdateAsync(Category category);
        Task<bool> UpdatePlaceAsync(List<Category> categorys);
        Task<int> GetLastPlaceAsync();
    }
}
