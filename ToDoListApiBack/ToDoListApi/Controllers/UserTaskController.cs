﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using ToDoListApi.Models;
using ToDoListApi.Request;
using ToDoListApi.Response;
using ToDoListApi.Services;

namespace ToDoListApi.Controllers
{
    [Route("api/[controller]")]
    public class UserTaskController : ControllerBase
    {
        private readonly IUserTaskService _userTaskService;

        public UserTaskController(IUserTaskService userTaskService)
        {
            _userTaskService = userTaskService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(await _userTaskService.GetAllAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = ex.Message
                });
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            UserTask userTask;
            try
            {
                userTask = await _userTaskService.GetAsync(id);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = ex.Message
                });
            }

            if (userTask == null)
            {
                return NotFound();
            }

            return Ok(userTask);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] UserTaskUpdateRequest request)
        {
            UserTask userTask;
            bool updated = false;

            try
            {
                userTask = await _userTaskService.GetAsync(id);
                userTask.Name = request.Name;
                userTask.Description = request.Description;
                userTask.CategoryId = request.CategoryId;
                updated = await _userTaskService.UpdateAsync(userTask);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = ex.Message
                });
            }

            if (!updated)
            {
                return BadRequest(new UserTaskUpdateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = "UserTask was not update"
                });
            }

            return Ok(new UserTaskUpdateResponse
            {
                Error = false,
                ErrorCode = 0,
                ErrorMessage = "",
                Content = JsonConvert.SerializeObject(await _userTaskService.GetAsync(id))
            });
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserTaskCreateRequest request)
        {
            UserTask userTask = new UserTask
            {
                Name = request.Name,
                Description = request.Description,
                CategoryId = request.CategoryId
            };

            bool create = false;

            try
            {
                create = await _userTaskService.CreateAsync(userTask);
            }
            catch(Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = ex.Message
                });
            }
            
            if (!create)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 11,
                    ErrorMessage = "Category was not create"
                });
            }

            UserTaskCreateResponse response = new UserTaskCreateResponse
            {
                Error = false,
                ErrorCode = 0,
                ErrorMessage = "",
                Content = JsonConvert.SerializeObject(userTask)
            };

            return CreatedAtAction(nameof(Get), new { id = userTask.Id }, response);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool deleted = false;

            try
            {
                deleted = await _userTaskService.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 10,
                    ErrorMessage = ex.Message
                });
            }

            if (!deleted)
            {
                return NotFound(new UserTaskDeleteResponse
                {
                    Error = true,
                    ErrorCode = 12,
                    ErrorMessage = "UserTask was not delete (not exist?)"
                });
            }

            return NoContent();
        }
    }
}
