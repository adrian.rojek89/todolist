﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ToDoListApi.Models;
using ToDoListApi.Request;
using ToDoListApi.Response;
using ToDoListApi.Services;

namespace ToDoListApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                return Ok(await _categoryService.GetAllWithUserTaskAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                Category category = await _categoryService.GetAsync(id);
                if (category == null)
                {
                    return NotFound();
                }

                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] CategoryUpdateRequest request)
        {
            bool updated = false;

            try
            {
                Category category = await _categoryService.GetAsync(id);
                category.Name = request.Name;
                updated = await _categoryService.UpdateAsync(category);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }

            if (!updated)
            {
                return BadRequest(new CategoryUpdateResponse
                {
                    Error = true,
                    ErrorCode = 3,
                    ErrorMessage = "Category was not update"
                });
            }

            return Ok(new CategoryUpdateResponse
            {
                Error = false,
                ErrorCode = 0,
                ErrorMessage = "",
                Content = JsonConvert.SerializeObject(await _categoryService.GetAsync(id))
            });
        }

        [HttpPut()]
        public async Task<IActionResult> UpdatePlace([FromBody] CategoryUpdatePlaceRequest request)
        {
            bool updated = false;

            try
            {
                updated = await _categoryService.UpdatePlaceAsync(request.Categorys);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }

            if (!updated)
            {
                return BadRequest(new CategoryUpdatePlaceResponse
                {
                    Error = true,
                    ErrorCode = 3,
                    ErrorMessage = "Category was not update"
                });
            }

            return Ok(new CategoryUpdatePlaceResponse
            {
                Error = false,
                ErrorCode = 0,
                ErrorMessage = "",
                Content = "Update was success"
            });
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CategoryCreateRequest request)
        {
            Category category;
            bool create = false;

            try
            {
                category = new Category
                {
                    Name = request.Name,
                    Place = await _categoryService.GetLastPlaceAsync() + 1
                };

                create = await _categoryService.CreateAsync(category);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }

            if (!create)
            {
                return BadRequest(new CategoryCreateResponse
                {
                    Error = true,
                    ErrorCode = 1,
                    ErrorMessage = "Category was not create"
                });
            }

            CategoryCreateResponse response = new CategoryCreateResponse
            {
                Error = false,
                ErrorCode = 0,
                ErrorMessage = "",
                Content = JsonConvert.SerializeObject(category)
            };

            return CreatedAtAction(nameof(Get), new { id = category.Id }, response);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool deleted = false;

            try
            {
                deleted = await _categoryService.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                return BadRequest(new UserTaskCreateResponse
                {
                    Error = true,
                    ErrorCode = 9,
                    ErrorMessage = ex.Message
                });
            }

            if (!deleted)
            {
                return NotFound(new CategoryDeleteResponse
                {
                    Error = true,
                    ErrorCode = 2,
                    ErrorMessage = "Category was not delete (not exist?)"
                });
            }

            return NoContent();
        }
    }
}
