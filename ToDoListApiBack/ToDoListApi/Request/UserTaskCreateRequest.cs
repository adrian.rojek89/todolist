﻿namespace ToDoListApi.Request
{
    public class UserTaskCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
    }
}
