﻿using System.Collections.Generic;
using ToDoListApi.Models;

namespace ToDoListApi.Request
{
    public class CategoryUpdatePlaceRequest
    {
        public List<Category> Categorys { get; set; }
    }
}
