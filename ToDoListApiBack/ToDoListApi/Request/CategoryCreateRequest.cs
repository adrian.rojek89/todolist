﻿namespace ToDoListApi.Request
{
    public class CategoryCreateRequest
    {
        public string Name { get; set; }
    }
}
