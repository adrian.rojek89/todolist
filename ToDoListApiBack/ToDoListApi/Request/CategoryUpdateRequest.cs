﻿namespace ToDoListApi.Request
{
    public class CategoryUpdateRequest
    {
        public string Name { get; set; }
    }
}
