import React from "react";
import styled from "styled-components";
import { ApiRequest } from "../ApiRequest/ApiRequest";

const ErrorMessage = styled.div`
  padding: 8px;
  font-size: 12;
  color: red;
`;

const TextArea = styled.textarea`
width: 100%;
  resize: none;
`;

const InputField = styled.input`
  width: 100%;
`;

const CreateUsetTaskForm = styled.form`
  padding: 8px;
`;

const initialState = {
  name: "",
  nameError: "",
  description: "",
  isEdit: false
};

export class UserTaskForm extends React.Component {
  constructor(props)
  {
    super(props);
    if(props.initialState !== undefined)
    {
      this.state = props.initialState;
    }
    else
    {
      this.state = initialState;
    }
   
    this.apiRequest = new ApiRequest();
  }
  
  handleRestRequest = response => {
    if(response.status === 201 || response.status === 200 || response.status === 204)
    {
      if(this.state.closePopup !== undefined)
      {
        this.state.closePopup();
      }

      this.props.forceUpdateParent();
    }
    else
    {
      alert(response.ErrorMessage);
    }
  };

  hangleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  validate = () => {
    let nameError = "";

    if (!this.state.name) {
      nameError = "name cannot be blank";
      this.setState({ nameError });
      return false;
    }

    return true;
  };

  handleSubmit = event => {
    event.preventDefault();
    const isValid = this.validate();
    if (isValid) {
     
      if(this.state.isEdit)
      {
        this.apiRequest.UpdateUserTask(this.state.userTaskId, this.state.name, this.state.description, 
          this.props.categoryId, this.handleRestRequest);
      }
      else
      {
        this.apiRequest.CreateUserTask(this.state.name, this.state.description, 
          this.props.categoryId, this.handleRestRequest);
      }

      console.log(this.state);
      // clear form
      this.setState(initialState);
    }
  };

  handleDelete = event => 
  {
    event.preventDefault();

    this.apiRequest.DeleteUserTask(this.state.userTaskId, this.handleRestRequest);

    console.log(this.state);
    // clear form
    this.setState(initialState);
  }

  render() {
    return (
      <CreateUsetTaskForm onSubmit={this.handleSubmit}>
        <ErrorMessage>{this.state.nameError}</ErrorMessage>
        <InputField
          name="name"
          placeholder="Category name..."
          value={this.state.name}
          onChange={this.hangleChange}
        />
        <br />
        <TextArea
          name="description"
          placeholder="Description..."
          value={this.state.description}
          onChange={this.hangleChange}
        />
        <br />
        <button type="submit">{this.state.isEdit ? "Update" : "Create"}</button>
        {this.state.isEdit ? <button onClick={this.handleDelete}>Delete</button>: null}
      </CreateUsetTaskForm>
    );
  }
}
