const ResultDnD = {
  draggableId: "task-1",
  type: "TYPE",
  reason: "DROP",
  source: {
    droppableId: "column-1",
    index: 0
  },
  destination: {
    droppableId: "column-2",
    index: 1
  }
};
