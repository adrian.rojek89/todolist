import React from "react";
import { DragDropContext } from "react-beautiful-dnd";
import "../css/CategoryList.css";
import { CategoryColumn } from "./CategoryColumn";
import { CategoryForm } from "./CategoryForm";
import { ApiRequest } from "../ApiRequest/ApiRequest";

export class CategoryList extends React.Component {
  constructor(props) {
    super(props);
    this.apiRequest = new ApiRequest();
    this.state = {
      categories: ""
    };

    this.onDragEnd = this.onDragEnd.bind(this);
    this.updateView = this.updateView.bind(this);
  }

  async componentDidMount() {
    const data = await this.apiRequest.GetAllCategory();
    this.setState({ categories: data });
  }

  handleRestRequest = response => {
    if (response.status !== 200) {
      alert(response.ErrorMessage);
      this.updateView();
    }
  };

  async onDragEnd(result) {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    const destinationIsChange =
      destination.droppableId !== source.droppableId &&
      destination.index !== source.index;

    if (!destinationIsChange) {
      return;
    }

    let categorySourceIndex = this.state.categories.findIndex(
      c => c.id === Number(source.droppableId)
    );
    let categorySource = this.state.categories[categorySourceIndex];
    let userTaskIndex = categorySource.userTasks.findIndex(
      u => u.id === Number(draggableId)
    );

    let userTask = categorySource.userTasks[userTaskIndex];
    userTask.categoryId = Number(destination.droppableId);

    categorySource.userTasks.splice(userTaskIndex, 1);

    let categoryDestinyIndex = this.state.categories.findIndex(
      c => c.id === Number(destination.droppableId)
    );
    let categoryDestiny = this.state.categories[categoryDestinyIndex];
    categoryDestiny.userTasks.push(userTask);

    this.setState({ categories: this.state.categories });
    this.forceUpdate();

    this.apiRequest.UpdateUserTask(
      userTask.id,
      userTask.name,
      userTask.description,
      userTask.categoryId,
      this.handleRestRequest
    );
  }

  async updateView() {
    const data = await this.apiRequest.GetAllCategory();
    this.setState({ categories: data });
    this.forceUpdate();
  }

  render() {
    return (
      <div>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div className="categoriesList">
            {this.state.categories
              ? this.state.categories.map(category => (
                  <div key={category.id}>
                    <CategoryColumn
                      category={category}
                      forceUpdateParent={this.updateView}
                    />
                  </div>
                ))
              : null}
            <CategoryForm forceUpdateParent={this.updateView} />
          </div>
        </DragDropContext>
      </div>
    );
  }
}
