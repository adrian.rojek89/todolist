import React from "react";
import styled from "styled-components";
import { UserTask } from "./UserTask";
import { Droppable } from "react-beautiful-dnd";
import { UserTaskForm } from './UserTaskForm';
import Popup from "reactjs-popup";
import { CategoryForm } from "./CategoryForm";
import "../css/CategoryList.css";

const CenterDiv = styled.div`
  margin-left: auto;
  margin-right: auto;
`;

const TaskList = styled.div`
  padding: 8px;
`;

export class CategoryColumn extends React.Component {

  constructor(props) {
    super(props);
    this.state = { open: false };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  createUserTask = (userTask, i) => {
    return (
      <div key={userTask.id}>
        <UserTask forceUpdateParent={this.props.forceUpdateParent} userTask={userTask} index={i} />
      </div>
    )
  };

  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }

  Edit = () => {
    this.openModal();
  }

  render() {
    return (
      <div className="category">
        <div onDoubleClick={this.Edit} className="categoryTitle">{this.props.category.name}</div>
        <Droppable droppableId={String(this.props.category.id)}>
          {provided => (
            <TaskList ref={provided.innerRef} {...provided.droppableProps}>
              {this.props.category.userTasks
              ? this.props.category.userTasks.map((userTask, i) => (
                  this.createUserTask(userTask,i)
                ))
              : null}
              {provided.placeholder}
            </TaskList>
          )}
        </Droppable>
        <UserTaskForm forceUpdateParent={this.props.forceUpdateParent} 
          categoryId={this.props.category.id}/>
        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div>
            <CenterDiv>Edit Category</CenterDiv>
            <CategoryForm initialState={{ name: this.props.category.name,
                      nameError: "",
                      categoryId: this.props.category.id,
                      isEdit: true,
                      closePopup: this.closeModal }} 
                forceUpdateParent={this.props.forceUpdateParent} />
          </div>
        </Popup>
      </div>
    );
  }
}
