import React from "react";
import { Draggable } from "react-beautiful-dnd";
import styled from "styled-components";
import { UserTaskForm } from './UserTaskForm';
import Popup from "reactjs-popup";

const Container = styled.div`
  border: 1px solid red;
  padding: 8px;
  border-radius: 2px;
  margin-bottom: 8px;
  background-color: white;
`;

const Title = styled.div`
  border: 1px solid red;
  background-color: white;
`;

const CenterDiv = styled.div`
  margin-left: auto;
  margin-right: auto;
`;

const Description = styled.div``;

export class UserTask extends React.Component {

  constructor(props) {
    super(props);
    this.state = { open: false };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }

  Edit = () => {
    this.openModal();
  }

  render() {
    return (
      <div>
        <Draggable draggableId={String(this.props.userTask.id)} index={this.props.userTask.categoryId * 100 + this.props.userTask.id}>
          {provided => (
            <Container onDoubleClick={this.Edit}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
          >
            <Title>{String(this.props.userTask.name)}</Title>
            <Description>{String(this.props.userTask.description)}</Description>
          </Container>
          )}
        </Draggable>
        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div>
            <CenterDiv>Edit User Task</CenterDiv>
            <UserTaskForm initialState={{ name: this.props.userTask.name,
                      nameError: "",
                      description: this.props.userTask.description,
                      userTaskId: this.props.userTask.id,
                      isEdit: true,
                      closePopup: this.closeModal }} 
                      forceUpdateParent={this.props.forceUpdateParent} categoryId={this.props.userTask.categoryId}/>
          </div>
        </Popup>
      </div>
    );
  }
}
