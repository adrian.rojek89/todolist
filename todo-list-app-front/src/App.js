import React from "react";
import "./App.css";
import { Header } from "./components/Header";
import { CategoryList } from "./components/CategoryList";

export class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div>
          <CategoryList />
        </div>
      </div>
    );
  }
}

export default App;
