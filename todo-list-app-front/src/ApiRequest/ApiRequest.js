import axios from "axios";

export class ApiRequest {
  url = "http://localhost:5000/api/";

  async GetAllCategory() {
    //pobieramy dane
    const response = await fetch(this.url + "Category/").catch(error => {
      alert(error);
    });

    //pobieramy json z danych
    return await response.json();
  }

  CreateCategory(name, callback) {
    axios
      .post(this.url + "Category/", {
        name: name
      })
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }

  UpdateCategory(categoryId, name, callback) {
    axios
      .put(this.url + "Category/" + categoryId, {
        name: name
      })
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }

  DeleteCategory(categoryId, callback) {
    axios
      .delete(this.url + "Category/" + categoryId)
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }

  CreateUserTask(name, description, categoryId, callback) {
    axios
      .post(this.url + "UserTask/", {
        name: name,
        description: description,
        categoryId: categoryId
      })
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }

  UpdateUserTask(userTaskId, name, description, categoryId, callback) {
    axios
      .put(this.url + "UserTask/" + userTaskId, {
        name: name,
        description: description,
        categoryId: categoryId
      })
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }

  DeleteUserTask(userTaskId, callback) {
    axios
      .delete(this.url + "UserTask/" + userTaskId)
      .then(response => callback(response))
      .catch(error => {
        alert(error);
      });
  }
}
